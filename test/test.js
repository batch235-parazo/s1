const { assert } = require("chai");
const { newUser } = require("../index.js")
const { user } = require("../index.js")

describe("Test newUser object", () => {
	it("Assert newUser type is an object", () => {
		assert.equal(typeof(newUser), "object");
	})

	// mini-activity
		// 1. Check if newUser email is type string
		// 2. Run the npm test in the terminal.
		// 3. Send your output in batch hangouts
	it("Assert newUser email is type string", () => {
		assert.equal(typeof(newUser.email), "string");
	})

	it("Assert newUser email is not undefined", () => {
		assert.notEqual(typeof(newUser.email), "undefined");
	})

	it("Assert newUser password is type string", () => {
		assert.equal(typeof(newUser.password), "string");
	})

	it("Assert newUser password is at least 16 characters long", () => {
		assert.isAtLeast(newUser.password.length, 16);
	})


});

describe("Test user object", () => {
	it("Assert user type is an object", () => {
		assert.equal(typeof(user), "object");
	})

	it("Assert user firstName is string", () => {
		assert.equal(typeof(user.firstName), "string");
	})

	it("Assert user lastName is string", () => {
		assert.equal(typeof(user.lastName), "string");
	})

	it("Assert user firstName is not undefined", () => {
		assert.notEqual(typeof(user.firstName), "undefined");
	})

	it("Assert user lastName is not undefined", () => {
		assert.notEqual(typeof(user.lastName), "undefined");
	})

	it("Assert user age is at least 18", () => {
		assert.isAtLeast(user.age, 18);
	})

	it("Assert user age is a number", () => {
		assert.equal(typeof(user.age), "number");
	})

	it("Assert user contact number is a string", () => {
		assert.equal(typeof(user.contactNumber), "string");
	})

	it("Assert user batchNumber is a number", () => {
		assert.equal(typeof(user.batchNumber), "number");
	})

	it("Assert user batchNumber is not undefined", () => {
		assert.notEqual(typeof(user.batchNumber), "undefined");
	})

	it("Assert user password is at least 16 characters long", () => {
		assert.isAtLeast(user.password.length, 16);
	})

	it("Assert user password is not undefined", () => {
		assert.notEqual(typeof(user.password), "undefined");
	})


})
