
const newUser = {
	email: "juan.delacruz@gmail.com",
	password: "thequickbrownfoxjumpsoverthelazydog"
}

// Activity: 
const user = {
	firstName: "John",
	lastName: "Dela Cruz",
	age: 18,
	contactNumber: "09123456789",
	batchNumber: 235,
	email: "john.delacruz@gmail.com",
	password: "sixteencharacters"
}

// Activity Instructions:
	/*1. Assert that the user firstName type is a string
	2. Assert that the user lastName type is a string
	3. Assert that the user firstName is not undefined
	4. Assert that the user lastName is not undefined.
	5. Assert that the user age is at least 18
	6. Assert that the user age type is a number.
	7. Assert that the user contact number type is a string
	8. Assert that the user batch number type is a number
	9. Assert that the user batch number is not undefined.
	10. Assert that the user password is at least 16 characters.
	11. Please assign a new test suite for the user */

module.exports = {
	newUser: newUser,
	user: user
}
